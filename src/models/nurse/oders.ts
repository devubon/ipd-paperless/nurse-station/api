import { Knex } from 'knex';
import { UUID } from 'crypto';

export class OdersModel {

    updateOrders(db: Knex, order_id: any, data: any) {
        return db('orders')
            .update(data)
            .where('id', order_id)
            .returning('*');
    }

}