import { UUID } from 'crypto';
import { Knex } from 'knex';
import patient from '../../routes/patient';

export class AdmitService {
  /**
   * บันทึกข้อมูล Admit
   * @param db 
   * @param data 
   * @returns 
   */
  save(db: Knex, data: object) {
    return db('admit')
      .insert(data).returning('*');
  }

  /**
   * 
   * @param db 
   * @param id 
   * @param data 
   * @returns 
   */
  update(db: Knex, id: UUID, data: object) {
    let sql = db('admit')
      .update(data)
      .where('id', id).returning('*');
    return sql
  }

  /**
   * 
   * @param db 
   * @param id 
   * @returns
   */
  delete(db: Knex, id: UUID) {
    let sql = db('admit')
      .delete()
      .where('id', id)
    return sql
  }

  /**
   * 
   * @param db 
   * @param wardId
   * @param query 
   * @param limit 
   * @param offset 
   * @returns 
   */
  infoActive(db: Knex, wardId: UUID, query: string, limit: number, offset: number) {
    let sql = db('admit as a')
      .select('a.*', 'p.cid', 'p.fname', 'p.lname', 'p.gender', 'p.age')
      .innerJoin('patient as p', 'p.admit_id', 'a.id')

    if (wardId) {
      sql.where(builder => {
        builder.whereRaw('a.ward_id = ?', [wardId])
      })
    }

    if (query) {
      let _query = `%${query}%`
      sql.where(builder => {
        builder.orWhere('p.cid', query)
          .orWhere('a.hn', query)
          .orWhereILike('p.fname', _query)
          .orWhereILike('p.lname', _query)
      })
    }

    return sql
      .where('a.is_active', true)
      .limit(limit).offset(offset);

  }

  infoActiveTotal(db: Knex, wardId: UUID, query: string) {
    let sql = db('admit as a')
      .innerJoin('patient as p', 'p.admit_id', 'a.id')

    if (wardId) {
      sql.where(builder => {
        builder.whereRaw('a.ward_id = ?', [wardId])
        
      })
    }

    if (query) {
      let _query = `%${query}%`
      sql.where(builder => {
        builder.orWhere('p.cid', query)
          .orWhere('a.hn', query)
          .orWhereILike('p.fname', _query) 
          .orWhereILike('p.lname', _query)
      })
    }

    return sql
      .where('a.is_active', true)
      .count({ total: '*' });

  }



  // Create By RENFIX การใช้ Transaction สำหรับบันทึกข้อมูลแบบ Array
  //viewIDByAN เอาไว้ดึง UUID ADMIT 
  viewIDByAN(db: Knex, admitAN: any) {    
    return db('admit')
      .select('id') // UUID
      .where('an', admitAN);
  }

  updateAdmitByID(db: Knex, dataAdmit: any, admit_id: any) {
    return db('admit')
      .update(dataAdmit)
      .where('id', admit_id)
      .returning('*');
  }

  insertAdmit(db: Knex, dataAdmit: any) {
    return db('admit')
      .insert(dataAdmit)
      .returning('*');
  }

  insertPatient(db: Knex, dataPartient: any) {
    return db('patient')
      .insert(dataPartient).returning('*');
  }

  updatePartient(db: Knex, dataPatient: any, admit_id: any) {
    return db('patient')
      .update(dataPatient)
      .where('admit_id', admit_id)
      .returning('*');
  }

  insertOpdReview(db: Knex, dataOpdReview: any) {
    return db('opd_review')
      .insert(dataOpdReview)
      .returning('*');
  }

  updateOpdReview(db: Knex, dataOpdReview: any, admit_id: any) {
    return db('opd_review')
      .update(dataOpdReview)
      .where('admit_id', admit_id)
      .returning('*');
  }

  insertTreatement(db: Knex, dataTreatment: any) {
    return db('opd_treatment')
      .insert(dataTreatment)
      .returning('*');
  }

  updateTreatement(db: Knex, dataTreatment: any, admit_id: any,item_code:any,item_type_id:any) {
    return db('opd_treatment')
      .update(dataTreatment)
      .where('admit_id', admit_id).andWhere('item_code', item_code).andWhere('item_type_id', item_type_id)
      .returning('*');
  }

  selectDepartment(db: Knex, department_code: string) {
    return db('h_department')
      .where('code', department_code)

  }


  insertPatientAllergy(db: Knex, dataPatient_allergy: any) {
    return db('patient_allergy')
      .insert(dataPatient_allergy)
  }


  updatePatientAllergy(db: Knex, dataPatient_allergy: any, patient_allergyAdmitID: any,drug_name:any) {
    return db('patient_allergy')
      .update(dataPatient_allergy)
      .where('admit_id', patient_allergyAdmitID).andWhere('drug_name',drug_name)
      .returning('*');
  }


  // AddAdmit(db: Knex, data1: any) {
  //   return new Promise((resolve: any, reject: any) => {
  //     db.transaction((trx) => {
  //       db('admit')
  //         .insert(data1)
  //         .returning('id')
  //         .onConflict(['an'])
  //         .merge()
  //         .transacting(trx)
  //         .then(trx.commit)
  //         .catch(trx.rollback);
  //     }).then(() => {
  //       resolve();
  //     })
  //       .catch((error: any) => {
  //         reject(error);
  //       });
  //   });
  // }


  infoByAdmitID(db: Knex, id: UUID) {
    let sql = db('admit as a')
      .leftJoin('patient as p', 'a.id', 'p.admit_id')
      .leftJoin('opd_review as o', 'a.id', 'o.admit_id')
      .select(
        'a.pre_diag','a.doctor_id','a.hn','a.an','a.ward_id','a.bed_id','p.title', 'p.fname', 'p.lname', 'p.gender',
        'p.age', 'p.address', 'p.phone', 'p.inscl_name',
        'o.chief_complaint', 'o.present_illness', 'o.past_history', 'o.physical_exam',
        'o.body_temperature', 'o.body_weight', 'o.body_height', 'o.waist', 'o.pulse_rate', 'o.respiratory_rate',
        'o.systolic_blood_pressure', 'o.diatolic_blood_pressure',
        'o.oxygen_sat', 'o.eye_score', 'o.movement_score', 'o.verbal_score'
      )
      .where('a.id', id);

    return sql
  }

  infoDischarge(db: Knex) {
    let sql = db('admit as a')
      .where('a.is_active', true);
    return sql
  }


  getPatient(db: Knex, id: UUID){
    return db('patient').where('admit_id', id);
  }
  getReview(db: Knex, id: UUID){
    return db('opd_review').where('admit_id', id);
  }
  getTreatement(db: Knex, id: UUID){
    return db('opd_treatment').where('admit_id', id);    
  }
  getAllergy(db: Knex, id: UUID){
    return db('patient_allergy').where('admit_id', id);
  }




}