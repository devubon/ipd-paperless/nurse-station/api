import { Knex } from 'knex';
import { UUID } from 'crypto';

export class DoctorOrderModel {

  getDoctorOrder(db: Knex, admit_id: any) {
    return db('admit')
      .leftJoin('doctor_order', 'doctor_order.admit_id', 'admit.id')
      .leftJoin('orders', 'orders.doctor_order_id', 'doctor_order.id')

      .where('admit.id', admit_id);
  }
// ปั้นก้อน DATA
  getDocterOrderadmitID(db: Knex, admit_id: any) {
    return db('doctor_order')
      .where('doctor_order.admit_id', admit_id);
  }


  getOrderByID(db: Knex, doctor_order_id: any) {
    return db('orders')
      .where('doctor_order_id', doctor_order_id);
  }

  getProgressNoteByID(db: Knex, doctor_order_id: any) {
    return db('progress_note')
      .where('doctor_order_id', doctor_order_id);
  }

  getOnedayByID(db: Knex, doctor_order_id: any) {
    return db('orders')
      .where('doctor_order_id', doctor_order_id)
      .andWhere('order_type_id',1);
  }

  getContinueByID(db: Knex, doctor_order_id: any) {
    return db('orders')
      .where('doctor_order_id', doctor_order_id)
      .andWhere('order_type_id',2);
  }

//----------------------------------------------//


  getByDoctorOrderID(db: Knex, doctorOrderID: UUID) {
    return db('orders')
      .where('doctor_order_id', doctorOrderID)
      .select(
        'id', 'doctor_order_id',
        'order_type_id', 'item_type_id', 'item_id', 'item_name',
        'medicine_usage_code', 'medicine_usage_extra',
        'quantity',
        'is_confirm', 'confirm_by', 'confirm_date', 'confirm_time',
        'is_process', 'process_by'

      )
  }

  insertDoctorOrder(db: Knex, dataDoctorOrder: any, doctor_order_id: any) {
    return db('doctor_order')
      .insert(dataDoctorOrder)
      .where('doctor_order_id', doctor_order_id)
      .returning('*');
  }

  insertProgressNote(db: Knex, dataProgressNote: any, doctor_order_id: any) {
    return db('progress_note')
      .insert(dataProgressNote)
      .where('progress_note', doctor_order_id)
      .returning('*');
  }

  insertOders(db: Knex, dataOders: any, doctor_order_id: any) {
    return db('oders')
      .insert(dataOders)
      .where('doctor_order_id', doctor_order_id)
      .returning('*');
  }

  insertOdersFood(db: Knex, dataOdersFood: any, doctor_order_id: any) {
    return db('oders_food')
      .insert(dataOdersFood)
      .where('order_food', doctor_order_id)
      .returning('*');
  }

  insertOrderVitalSign(db: Knex, dataOrderVitalSign: any) {
    return db('order_vital_sign')
      .insert(dataOrderVitalSign)
      .where('order_vital_sign', dataOrderVitalSign)
      .returning('*');

  }

  

}