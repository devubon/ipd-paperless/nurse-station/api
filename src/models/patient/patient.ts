import { UUID } from 'crypto';
import { Knex } from 'knex';
export class PatientModel {
  /**
   * บันทึกข้อมูล Admit
   * @param db 
   * @param data 
   * @returns 
   */
  info(db: Knex,data:any) {
    let sql = db('patient')
    return sql.orderBy('an')
    .where('an',data)
  }

  infoTotal(db: Knex) {
    let sql = db('patient')
    return sql.count({ total: '*' });
  }

  infoByID(db: Knex,id:UUID) {
    let sql = db('patient')
    return sql.where('id',id).orderBy('an')
  }

  infoByAN(db: Knex,an:any) {
    let sql = db('patient')
    return sql.where('an',an).orderBy('an')
  }

  /**
   * 
   * @param db 
   * @param data 
   * @returns 
   */
  save(db: Knex,data:object){
    let sql = db('patient')
    return sql.insert(data);
  }

  /**
   * 
   * @param db 
   * @param data 
   * @param id 
   * @returns 
   */
  update(db: Knex,data:object,id:UUID){
    let sql = db('bepatientd')
    return sql.update(data).where('id',id);
  }
  /**
   * 
   * @param db 
   * @param id 
   * @returns 
   */
  delete(db: Knex,id:UUID){
    let sql = db('patient')
    return sql.delete().where('id',id);
  }

}