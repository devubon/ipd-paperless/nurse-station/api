import { Knex } from 'knex';
export class AdmitService {
  /**
   * บันทึกข้อมูล Admit
   * @param db 
   * @param data 
   * @returns 
   */
  create(db: Knex, data: object) {
    return db('admit')
      .insert(data);
  }

  demoTransaction(db: Knex, id: any, wardId: any, data: any) {
    return new Promise((resolve: any, reject: any) => {
      db.transaction((trx) => {
        db('admit')
          .update('ward_id', wardId)
          .where('id', id)
          .transacting(trx)
          .then(() => {
            // insert
            return db('patient_move')
              .insert(data)
              .then(trx.commit)
              .catch(trx.rollback);
          })

      }).then(() => {
        resolve();
      })
        .catch((error: any) => {
          reject(error);
        })
    })
  }
}