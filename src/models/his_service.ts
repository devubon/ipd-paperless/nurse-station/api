import { AxiosInstance } from "axios";
export class HISService {
  getWaitingList(axios: AxiosInstance, token: any, an: any = '',dateStart:any,dateEnd:any) {
    const url: any = `/services/patient?an=${an}&dateStart=${dateStart}&dateEnd=${dateEnd}`;
    return axios.get(url, {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    });
  }

  getWaitingListReview(axios: AxiosInstance, token: any, an: any = '') {
    const url: any = `/services/review?an=${an}`;
    return axios.get(url, {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    });
  }

  getWaitingListTreatement(axios: AxiosInstance, token: any, an: any = '') {
    const url: any = `/services/treatement?an=${an}`;
    return axios.get(url, {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    });
  }  

  getWaitingListAdmit(axios: AxiosInstance, token: any, an: any = '') {
    const url: any = `/services/admit?an=${an}`;
    return axios.get(url, {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    });
  }  

  getWaitingListPatientAllergy(axios: AxiosInstance, token: any, an: any = '') {
    const url: any = `/services/patient-allergy?an=${an}`;
    return axios.get(url, {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    });
  }  

  getWaitingListLabResult(axios: AxiosInstance, token: any, an: any = '',code: any ='') {
    const url: any = `/services/lab-result?an=${an}&code=${code}`;
    return axios.get(url, {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    });
  } 

  getWaitingListLabFinding(axios: AxiosInstance, token: any, an: any = '',code: any ='') {
    const url: any = `/services/lab-finding?an=${an}`;
    return axios.get(url, {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    });
  } 

  getWaitingListXrayResult(axios: AxiosInstance, token: any, an: any = '') {
    const url: any = `/services/xray-result?an=${an}`;
    return axios.get(url, {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    });
  }

  getWaitingListEkg(axios: AxiosInstance, token: any, an: any = '') {
    const url: any = `/services/ekg?an=${an}`;
    return axios.get(url, {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    });
  }
}