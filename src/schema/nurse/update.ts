import S from 'fluent-json-schema'

const schema = S.object()
  .prop('insurance_id', S.string().format('uuid'))
  .prop('department_id', S.string().format('uuid'))
  .prop('pre_diag', S.string().maxLength(250))
  .prop('ward_id', S.string().format('uuid'))
  .prop('bed_id', S.string().format('uuid'))
  .prop('doctor_id', S.string().format('uuid'))

export default {
  body: schema
}