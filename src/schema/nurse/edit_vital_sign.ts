import S from 'fluent-json-schema'

//Create By RENFIX 
const schema = S.object()
  .prop('admit_id', S.string().format('uuid').required())
 // .prop('vital_sign_date', S.string().format('date').required())
 // .prop('vital_sign_time', S.string().format('time').required())
  .prop('pulse_rate', S.integer().required())
  .prop('respiratory_rate', S.integer().required())
  .prop('systolic_blood_pressure', S.integer().required())
  .prop('diatolic_blood_pressure', S.integer().required())
  .prop('intake_oral_fluid', S.integer().required())
  .prop('intake_penterate', S.integer().required())
  .prop('intake_medicine', S.integer().required())
  .prop('outtake_urine', S.integer().required())
  .prop('outtake_emesis', S.integer().required())
  .prop('outtake_drainage', S.integer().required())
  .prop('outtake_aspiration', S.integer().required())
  .prop('outtake_lochia', S.integer().required())
  .prop('stools', S.integer().required())
  .prop('urine', S.integer().required())
  .prop('pain_score', S.integer().required())
  .prop('oxygen_sat', S.integer().required())
  .prop('body_weight', S.number().required())
  //.prop('create_at', S.integer().required())
  // .prop('create_by', S.string().format('uuid').required())
  // .prop('modify_at', S.integer().required())
  .prop('modify_by', S.string().format('uuid').required())

 
 

export default {
  body: schema
}