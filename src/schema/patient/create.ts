import S from 'fluent-json-schema'

const schema = S.object()
  .prop('an', S.string().maxLength(15).required())
  .prop('hn', S.string().maxLength(15).required())
  .prop('cid', S.string().maxLength(13))
  .prop('title', S.string().maxLength(50))
  .prop('fname', S.string().maxLength(50))
  .prop('lname', S.string().maxLength(50))
  .prop('gender', S.string().maxLength(50))
  .prop('nationality', S.string().maxLength(50))
  .prop('citizenchip', S.string().maxLength(50))
  .prop('religion', S.string().maxLength(50))
  .prop('dob', S.string().format('date'))
  .prop('age', S.string().maxLength(10))
  .prop('occupation', S.string().maxLength(100))
  .prop('inscl', S.string().maxLength(100))
  .prop('address', S.string().maxLength(500))
  .prop('phone', S.string().maxLength(50))
  .prop('is_pregnant', S.boolean().default(false))
  // .prop('reference_person_name', S.string().maxLength(50))
  // .prop('reference_person_address', S.string().maxLength(50))
  // .prop('reference_person_phone', S.string().maxLength(50))
  // .prop('reference_person_relationship', S.string().maxLength(50))
  // .prop('insurance_id', S.string().format('uuid').required())

export default {
  body: schema
}