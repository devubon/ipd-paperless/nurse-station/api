import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { DateTime } from 'luxon';
import _ from 'lodash';
//import { DateTime } from 'luxon';
import { NurseNoteModel } from '../../models/nurse/nurse-note';
import addSchema from '../../schema/nurse/add_nurse_note';
import viewNurseNote from '../../schema/nurse/view_nurse_note';
import { ActivityModel } from '../../models/nurse/activity';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

    const db = fastify.db;
    const nurseNote = new NurseNoteModel();
    const activityModel = new ActivityModel

    // get patient
  fastify.get('/:admit_id', {
    preHandler: [
      fastify.guard.role('nurse', 'admin', 'doctor'),
      // fastify.guard.scope('nurse.create', 'admit.create', 'nurse.read', 'admit.read')
    ],
    schema: viewNurseNote
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      const params: any = request.params;
      const {admit_id} = params;

      const data = await nurseNote.getNurseNote(db, admit_id);

      return reply.status(StatusCodes.CREATED).send({ ok: true,data });

    } catch (error: any) {
      request.log.info(error.message);
       //return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
       return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, message: 'มีการเข้าถึงข้อมูลโดยไม่ผ่านการตรวจสอบ Schema' });
    }
  });

  fastify.put('/:id', {
    preHandler: [
      fastify.guard.role('nurse', 'admin', 'doctor'),
      // fastify.guard.scope('nurse.create', 'admit.create', 'nurse.read', 'admit.read')
    ],
    schema: addSchema
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      const req: any = request
      const data: any = req.body
      const id: any = req.params.id

      await nurseNote.update(db, id, data)
      return reply.status(StatusCodes.OK).send({ ok: true });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  });

// Create By RENFIX insert nurse_note
fastify.post('/', {
  preHandler: [
    fastify.guard.role('nurse', 'admin', 'doctor'),
  ],
  schema: addSchema,
}, async (request: FastifyRequest, reply: FastifyReply) => {
  try {
      const body: any = request.body;
      const {
          nurse_note_date,
          nurse_note_time,
          problem_list, //เป็น Json
          activity, //เป็น Json
          evaluate, //เป็น Json
          admit_id,
          item_used
      } = body;

      const now = DateTime.now().setZone('Asia/Bangkok');
     
      const userId = request.user.sub;


      const data: any = {
          'nurse_note_date': nurse_note_date,
          'nurse_note_time': nurse_note_time,
          'problem_list': problem_list, //เป็น Json
          'activity': activity, //เป็น Json
          'evaluate': evaluate, //เป็น Json
          'create_date': now,
          'create_by': userId,
          'modify_date': now,
          'modify_by': userId,
          'admit_id':admit_id,
          'item_used': item_used

      }

      await activityModel.insertNurseNote(db, data);
      return reply.status(StatusCodes.OK)
          .send({ status: 'ok' });
  } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
          .send({
              status: 'error',
              error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
          });
  }
});

  done();

}

