import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import _ from 'lodash';
import { DateTime } from 'luxon';

import { VitalSignModel } from '../../models/nurse/vital_sign';
import add_vital_sign from '../../schema/nurse/add_vital_sign';
import put_vital_sign from '../../schema/nurse/put_vital_sign';
export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db = fastify.db;
  const vitalSignModel = new VitalSignModel();

  // add VitalSign Create by RENFIX
  fastify.post('/', {
    preHandler: [
      fastify.guard.role('nurse', 'admin', 'doctor'),
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    schema: add_vital_sign
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      const body: any = request.body;
      const data: any = {...body}
      // const { //id, ไม่เอาเนื่องจาก database จะ genมาให้เป็น uuid
      //   admit_id,
      //   vital_sign_date,
      //   vital_sign_time,
      //   body_temperature,
      //   pulse_rate,
      //   respiratory_rate,
      //   systolic_blood_pressure,
      //   diatolic_blood_pressure,
      //   intake_oral_fluid,
      //   intake_penterate,
      //   intake_medicine,
      //   outtake_urine,
      //   outtake_emesis,
      //   outtake_drainage,
      //   outtake_aspiration,
      //   outtake_lochia,
      //   stools,
      //   urine,
      //   pain_score,
      //   oxygen_sat,
      //   body_weight,
      //   // create_at,
      //   create_by,
      //   // modify_at,
      //   // modify_by,
      //   is_active,
      //   round_time

      // } = body;

      const signTimeStampNow = DateTime.now().setZone('Asia/Bangkok');
      const signDateNow = DateTime.now().toSQL();
      const signTimeNow = DateTime.now().toFormat('HH:mm:ss');
      // const data: any = {
      //   // 'id': id, ไม่เอาเนื่องจาก database จะ genมาให้เป็น uuid
      //   'admit_id': admit_id,
      //   'vital_sign_date': vital_sign_date,
      //   'vital_sign_time': vital_sign_time,
      //   'body_temperature': body_temperature,
      //   'pulse_rate': pulse_rate,
      //   'respiratory_rate': respiratory_rate,
      //   'systolic_blood_pressure': systolic_blood_pressure,
      //   'diatolic_blood_pressure': diatolic_blood_pressure,
      //   'intake_oral_fluid': intake_oral_fluid,
      //   'intake_penterate': intake_penterate,
      //   'intake_medicine': intake_medicine,
      //   'outtake_urine': outtake_urine,
      //   'outtake_emesis': outtake_emesis,
      //   'outtake_drainage': outtake_drainage,
      //   'outtake_aspiration': outtake_aspiration,
      //   'outtake_lochia': outtake_lochia,
      //   'stools': stools,
      //   'urine': urine,
      //   'pain_score': pain_score,
      //   'oxygen_sat': oxygen_sat,
      //   'body_weight': body_weight,
      //   'create_at': signTimeStampNow,
      //   'create_by': create_by,
      //   // 'modify_at': signTimeStampNow,
      //   // 'modify_by': modify_by ,
      //   'is_active': is_active,
      //   'round_time': round_time

      // }


      await vitalSignModel.insertVitalSign(db, data);
      return reply.status(StatusCodes.OK)
        .send({ status: 'ok' });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  // get patient
  fastify.get('/:admit_id', {
    preHandler: [
      fastify.guard.role('nurse', 'admin', 'doctor'),
      // fastify.guard.scope('nurse.create', 'admit.create', 'nurse.read', 'admit.read')
    ],
    // schema: anSchema
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      const params: any = request.params;
      const { admit_id } = params;

      const data_admit: any[] = await vitalSignModel.getVitalSignByTableAdmit(db, admit_id);
     
      for (let a of data_admit ) {
        
        let data_vital_sign = await vitalSignModel.getVitalSignByTableVital_Sign(db,admit_id);

        if (data_vital_sign[0]) {

          a.data_vital_sign = data_vital_sign;

        } else {
          a.data_vital_sign = [];

        }
    }
      
      
      //const vital_sign = await vitalSignModel.getVitalSignByTableVital_Sign(db, admit_id);
      return reply.status(StatusCodes.CREATED).send({ ok: true, data_admit });
      

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  });

  fastify.put('/:vital_sign_id/user/:user_id', {
    preHandler: [
      fastify.guard.role('nurse', 'admin', 'doctor'),
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    schema: put_vital_sign
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {


      const params: any = request.params;
      const { vital_sign_id } = params;
      const { user_id } = params;

      const body: any = request.body;
      const { //id, ไม่เอาเนื่องจาก database จะ genมาให้เป็น uuid
        admit_id,
        vital_sign_date,
        vital_sign_time,
        body_temperature,
        pulse_rate,
        respiratory_rate,
        systolic_blood_pressure,
        diatolic_blood_pressure,
        intake_oral_fluid,
        intake_penterate,
        intake_medicine,
        outtake_urine,
        outtake_emesis,
        outtake_drainage,
        outtake_aspiration,
        outtake_lochia,
        stools,
        urine,
        pain_score,
        oxygen_sat,
        body_weight,
        // create_at,
        // create_by,
        // modify_at,
        modify_by

      } = body;

      const signTimeStampNow = DateTime.now().setZone('Asia/Bangkok');
      // const signDateNow = DateTime.now().toSQL();
      // const signTimeNow = DateTime.now().toFormat('HH:mm:ss');
      const data: any = {
        'id': vital_sign_id, 
        'admit_id': admit_id,
        'vital_sign_date': vital_sign_date,
        'vital_sign_time': vital_sign_time,
        'body_temperature': body_temperature,
        'pulse_rate': pulse_rate,
        'respiratory_rate': respiratory_rate,
        'systolic_blood_pressure': systolic_blood_pressure,
        'diatolic_blood_pressure': diatolic_blood_pressure,
        'intake_oral_fluid': intake_oral_fluid,
        'intake_penterate': intake_penterate,
        'intake_medicine': intake_medicine,
        'outtake_urine': outtake_urine,
        'outtake_emesis': outtake_emesis,
        'outtake_drainage': outtake_drainage,
        'outtake_aspiration': outtake_aspiration,
        'outtake_lochia': outtake_lochia,
        'stools': stools,
        'urine': urine,
        'pain_score': pain_score,
        'oxygen_sat': oxygen_sat,
        'body_weight': body_weight,
        // 'create_at': signTimeStampNow,
        // 'create_by': create_by,
        'modify_at': signTimeStampNow,
        'modify_by': modify_by


      }

      await vitalSignModel.insertVitalSign(db, data);
      return reply.status(StatusCodes.OK)
        .send({ status: 'ok' });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });


  done();

}

