import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';

import { UUID } from 'crypto';

import { AdmitService } from '../../models/nurse/admit';
import createSchema from '../../schema/nurse/create';
import updateSchema from '../../schema/nurse/update';
import listSchema from '../../schema/nurse/list';
import { lstatSync } from 'fs';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const admitService = new AdmitService();

  fastify.post('/', {
    preHandler: [
      fastify.guard.role('nurse', 'admin', 'doctor'),
      // fastify.guard.scope('nurse.create', 'admit.create', 'nurse.read', 'admit.read')
    ],
    schema: createSchema
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      // jwt decoded
      const userId: any = request.user.sub;

      const body: any = request.body;
      const {
        an,
        hn,
        vn,
        insurance_id,
        department_id,
        pre_diag,
        ward_id,
        bed_id,
        admit_date,
        admit_time,
        doctor_id,
        admit_by
      } = body;

      const data: any = {
        an,
        hn,
        vn,
        insurance_id,
        department_id,
        pre_diag,
        ward_id,
        bed_id,
        admit_date,
        admit_time,
        doctor_id,
        admit_by,
        create_by: userId
      }

      let rs = await admitService.save(db, data);

      return reply.status(StatusCodes.CREATED).send({ ok: true ,data:rs});

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })

  fastify.put('/:id', {
    preHandler: [
      fastify.guard.role('nurse', 'admin', 'doctor'),
      // fastify.guard.scope('nurse.create', 'admit.create', 'nurse.read', 'admit.read')
    ],
    schema: updateSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      const req: any = request
      const data: any = req.body
      const id: any = req.params.id

      let rs = await admitService.update(db, id, data)
      return reply.status(StatusCodes.OK).send({ ok: true,data: rs });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  })
 
  fastify.delete('/:id', {
    preHandler: [
      fastify.guard.role('nurse', 'admin', 'doctor'),
      // fastify.guard.scope('nurse.create', 'admit.create', 'nurse.read', 'admit.read')
    ],
    // schema: updateSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      const req: any = request
      const id: any = req.params.id

      await admitService.delete(db, id)
      return reply.status(StatusCodes.OK).send({ ok: true });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  })

  fastify.get('/', {
    preHandler: [
      fastify.guard.role('nurse', 'admin', 'doctor'),
    // fastify.guard.scope('nurse.ward')
  ],
    schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const req: any = request.query
      const ward_id: any = req.ward_id
      const query = req.query
      const limit: any = req.limit
      const offset: any = req.offset

      const results: any = await admitService.infoActive(db, ward_id, query, limit, offset);

      // total
      const resultTotal: any = await admitService.infoActiveTotal(db, ward_id, query)

      return reply.status(StatusCodes.OK)
        .send({
          ok: true,
          data: results,
          total: Number(resultTotal[0].total)
        });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  })

  fastify.get('/:id', {
    preHandler: [
      fastify.guard.role('nurse', 'admin', 'doctor'),
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const req: any = request.params
      const id: any = req.id

      const data: any = await admitService.infoByAdmitID(db, id)

      return reply.status(StatusCodes.OK)
        .send({
          ok: true,
          data,
        });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  })

  fastify.get('/is_admit', {
    preHandler: [
      fastify.guard.role('nurse', 'admin', 'doctor'),
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      const data: any = await admitService.infoDischarge(db)
      return reply.status(StatusCodes.OK)
        .send({
          ok: true,
          data,
        });


    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  })



  fastify.post('/upsert-admit', {
    preHandler: [
      fastify.guard.role('nurse', 'admin', 'doctor'),      
    ], //เช็คว่าเป็น Admin หรือไม่ 

  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      let errors:any =[];
      const body: any = request.body;
      const data: any = { ...body };// รับ body ทั้งหมดที่ frontend ส่งมา
      let dataAdmit: any = data['admit'];// ดึงเอาเฉพาะ table admit
      let dataPatient: any = data['patient'];// ดึงเอาเฉพาะ table patient
      let dataOpdReview: any = data['review'];// ดึงเอาเฉพาะ table review
      let dataTreatment: any = data['treatment'];// ดึงเอาเฉพาะ table treatment
      let dataBed: any = data['bed']; //ดึงเอาเฉพาะ table bed
      let dataWardID: any = data['ward']; //ดึงเอาเฉพาะ table wawrd
      let dataDepartmentID: any = data['department']; //ดึงเอาเฉพาะ table department
      let dataPatient_allergy: any = data['patient_allergy'];// ดึงเอาเฉพาะ table allergy
      let dataDoctor: any = data['doctor'];// ดึงเอาเฉพาะ table allergy
      //console.log('sssssss', dataDepartmentID);
      //console.log('ดึงเฉพาะ patienr',dataPatient);

      // let admitAN: any = dataAdmit[0].an; // ดึงเอาเฉพาะ an ใน table admit

      // let patient_allergyAN: any =dataPatient_allergy[0].an; //ใช้ admit_id เป็นคีย์อับเดท
      //let itemAdmit: any;

      // หา admit_id
      // let rsAdmit: any = await admitService.viewIDByAN(db, dataAdmit[0].an);
       let _rsAdmit: any;

      // if (rsAdmit[0]) {
      //   if (!dataOpdReview[0].eye_score) {
      //     delete dataOpdReview[0].eye_score;
      //     dataOpdReview[0].eye_score = null;
      //   }
      //   if (!dataOpdReview[0].movement_score) {
      //     delete dataOpdReview[0].movement_score;
      //     dataOpdReview[0].movement_score = null;
      //   }
      //   if (!dataOpdReview[0].verbal_score) {
      //     delete dataOpdReview[0].verbal_score;
      //     dataOpdReview[0].verbal_score = null;
      //   }


      //   delete dataPatient[0].admit_date;
      //   delete dataPatient[0].admit_time;
      //   delete dataPatient[0].pre_diag;
      //   delete dataPatient[0].admit_by;

      //   delete dataAdmit[0].inscl_code;
      //   delete dataAdmit[0].ward_code;
      //   delete dataAdmit[0].bed_code;
      //   delete dataAdmit[0].department_code;

      //   delete dataPatient_allergy[0].an;


      //   dataAdmit[0].ward_id = dataWardID[0].ward_id;
      //   dataAdmit[0].bed_id = dataBed[0].bed_id;
      //   dataAdmit[0].inscl = dataAdmit[0].inscl_code;
      //   dataAdmit[0].doctor_id = dataDoctor[0].user_id;

      //   dataAdmit[0].department_id = dataDepartmentID[0].department_id;

      //   // หา แต่ละตารางว่ามีข้อมูลไหม
      //   let rsPatientID: any = await admitService.getPatient(db, rsAdmit[0].id);
      //   let rsReviewID: any = await admitService.getReview(db, rsAdmit[0].id);
      //   let rsTreatementID: any = await admitService.getTreatement(db, rsAdmit[0].id);
      //   let rsAllergyID: any = await admitService.getAllergy(db, rsAdmit[0].id);


      //   // สั่ง insert update แต่ตาราง
      //   await admitService.updateAdmitByID(db, dataAdmit[0], rsAdmit[0].id);

      //   if (rsPatientID[0]) {
      //     await admitService.updatePartient(db, dataPatient[0], rsAdmit[0].id);
      //   } else {
      //     dataPatient[0].admit_id = rsAdmit[0].id;
      //     await admitService.insertPatient(db, dataPatient);
      //   }

      //   if (rsReviewID[0]) {
      //     await admitService.updateOpdReview(db, dataOpdReview[0], rsAdmit[0].id);
      //   } else {
      //     dataOpdReview[0].admit_id = rsAdmit[0].id;
      //     await admitService.insertOpdReview(db, dataOpdReview);
      //   }

      //   if (rsTreatementID[0]) {
      //     for (let i of dataTreatment) {
      //       i.admit_id = rsAdmit[0].id;
      //       await admitService.updateTreatement(db, i, i.admit_id, i.item_code, i.item_type_id);
      //     }

      //   } else {
      //     for (let i of dataTreatment) { i.admit_id = rsAdmit[0].id; }
      //     await admitService.insertTreatement(db, dataTreatment);
      //   }

      //   if (rsAllergyID[0]) {
      //     for (let i of dataPatient_allergy) {
      //       i.admit_id = rsAdmit[0].id;
      //       await admitService.updatePatientAllergy(db, i, i.admit_id, i.drug_name);
      //     }
      //     // await admitService.updatePatientAllergy(db, dataPatient_allergy[0], rsAdmit[0].id ,dataPatient_allergy.drug_name);
      //   } else {
      //     for (let i of dataPatient_allergy) { i.admit_id = rsAdmit[0].id; }
      //     await admitService.insertPatientAllergy(db, dataPatient_allergy);
      //   }
      //   return reply.status(StatusCodes.OK).send({ ok: true, rsAdmit: rsAdmit });

      // } else {

        // let rsDep = await admitService.selectDepartment(db, dataAdmit[0].department_code)
        let rsInscl: any = dataAdmit[0].inscl_code;

        if (dataPatient[0]) {
          delete dataPatient[0].admit_date;
          delete dataPatient[0].admit_time;
          delete dataPatient[0].pre_diag;
          delete dataPatient[0].admit_by;
        }

        if (dataAdmit[0]) {
          delete dataAdmit[0].inscl_code;
          delete dataAdmit[0].ward_code;
          delete dataAdmit[0].bed_code;
          delete dataAdmit[0].department_code;
        }

        if (dataPatient_allergy[0]){
          delete dataPatient_allergy[0].an;
        }

        if(dataOpdReview[0]){
          if (dataOpdReview[0].eye_score == '') {
            delete dataOpdReview[0].eye_score;
            dataOpdReview[0].eye_score = null;
          }
          if (dataOpdReview[0].movement_score == '') {
            delete dataOpdReview[0].movement_score;
            dataOpdReview[0].movement_score = null;
          }
          if (dataOpdReview[0].verbal_score == '') {
            delete dataOpdReview[0].verbal_score;
            dataOpdReview[0].verbal_score = null;
          }  
        }

        if(dataWardID){
          dataAdmit[0].ward_id = dataWardID[0].ward_id;
        }
        if(dataBed){
          dataAdmit[0].bed_id = dataBed[0].bed_id;
          dataAdmit[0].bed_number = dataBed[0].bed_number;
        }
        if(dataDoctor){
          dataAdmit[0].doctor_id = dataDoctor[0].user_id;
        }
        if(dataAdmit[0]){
          dataAdmit[0].inscl = rsInscl;
        }
        if(dataDepartmentID){
          dataAdmit[0].department_id = dataDepartmentID[0].department_id;
        }

        _rsAdmit = await admitService.insertAdmit(db, dataAdmit);


        console.log(_rsAdmit);
        dataPatient[0].admit_id = _rsAdmit[0].id; //แทรกฟิว admit_id ลงไปในคิวรี่
        dataOpdReview[0].admit_id = _rsAdmit[0].id;
        for (let i of dataTreatment) {
          i.admit_id = _rsAdmit[0].id;
        } //แทรกฟิว admit_id ลงไปในคิวรี่
        for (let i of dataPatient_allergy) { i.admit_id = _rsAdmit[0].id; }

        if(dataPatient[0]){
          await admitService.insertPatient(db, dataPatient);
        }
        if(dataOpdReview[0]){
          await admitService.insertOpdReview(db, dataOpdReview);
        }
        if(dataTreatment[0]){
          await admitService.insertTreatement(db, dataTreatment);
        }
        if(dataPatient_allergy[0]){
          await admitService.insertPatientAllergy(db, dataPatient_allergy);
        }

        return reply.status(StatusCodes.OK).send({ ok: true, rsAdmit: _rsAdmit });

     // }

    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });



  done();

}
