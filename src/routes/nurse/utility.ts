import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import _ from 'lodash';
import { DateTime } from 'luxon';
import { UtilityModel } from '../../models/nurse/utility';

import changeWardSchema from '../../schema/nurse/change_ward';
import changeBedSchema from '../../schema/nurse/assign_bed';



export default async (fastify: FastifyInstance, _options: any, done: any) => {

    const db = fastify.db;
    const utilityModel = new UtilityModel();

    fastify.put('/change-ward/:id', {
        preHandler: [
            fastify.guard.role('nurse', 'admin', 'doctor'),
        ], //เช็คว่าเป็น Admin หรือไม่ 
        schema: changeWardSchema,
    }, async (request: FastifyRequest, reply: FastifyReply) => {
        try {
            const params: any = request.params;
            const { id } = params;

            const body: any = request.body;
            const { ward_id } = body;

            const old_ward_id: any = await utilityModel.selectOldWard(db, id);

            if (old_ward_id[0].ward_id == null) {
                // console.log("วิงมาddddddddddd if  null แล้ว");
                try {

                    await utilityModel.assignWardWithNoWard(db, id, ward_id);
                    return reply.status(StatusCodes.OK)
                        .send({ status: 'ok' });

                } catch (error: any) {
                    request.log.error(error);
                    return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
                        .send({
                            status: 'error',
                            error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
                        });

                }

            }



            const data: any = {
                'ward_id': ward_id

            }
            const now = DateTime.now().setZone('Asia/Bangkok');
            const moveDate = DateTime.now().toSQL();

            const moveTime = DateTime.now().toFormat('HH:mm:ss');
            const userId = request.user.sub;

            const data2: any = {
                'admit_id': id,
                'move_date': moveDate,
                'move_time': moveTime,
                'create_date': now,
                'create_by': userId,
                'modify_by': userId,
                'move_type_id': 2,
                'old_id': old_ward_id[0].ward_id
            }


            //ส่งพารามิเตอร์ db,Id,wardId,data ไป utilityModel
            await utilityModel.changeWardTransaction(db, id, data, data2);
            return reply.status(StatusCodes.OK)
                .send({ status: 'ok' });
        } catch (error: any) {
            request.log.error(error);
            return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
                .send({
                    status: 'error',
                    error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
                });
        }
    });


    fastify.put('/change-bed-type/:id', {
        preHandler: [
            fastify.guard.role('nurse', 'admin', 'doctor'),
        ], //เช็คว่าเป็น Admin หรือไม่ 
        schema: changeBedSchema,
    }, async (request: FastifyRequest, reply: FastifyReply) => {
        try {
            const params: any = request.params;
            const { id } = params;

            const body: any = request.body;
            let { bed_id } = body;

            const old_id: any = await utilityModel.selectOldBed(db, id);
            //console.log('ssssssssssssssssssss', old_id);

            if (old_id[0].bed_id == null) {
                // console.log("วิงมาddddddddddd if  null แล้ว");
                try {

                    await utilityModel.assignBedWithNoBed(db, id, bed_id);
                    return reply.status(StatusCodes.OK)
                        .send({ status: 'ok' });

                } catch (error: any) {
                    request.log.error(error);
                    return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
                        .send({
                            status: 'error',
                            error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
                        });

                }

            }
            //console.log("ออกจาก IF เพราะว่า old_id ไม่ว่าง");

            const data: any =  {'bed_id':bed_id}
            const data_oldbed_id: any = old_id[0].bed_id;
            console.log('ข้อมูล data',data );
          

            const now = DateTime.now().setZone('Asia/Bangkok');
            const moveDate = DateTime.now().toSQL();

            const moveTime = DateTime.now().toFormat('HH:mm:ss');
            const userId = request.user.sub;

            const data2: any = {
                'admit_id': id,
                'move_date': moveDate,
                'move_time': moveTime,
                'create_date': now,
                'create_by': userId,
                'modify_by': userId,
                'move_type_id': 3,
                'old_id': old_id[0].bed_id
            }


             //ส่งพารามิเตอร์ db,Id,wardId,data ไป utilityModel
            await utilityModel.assignBedTypeTransaction(db, id, data, data2, data_oldbed_id);
            return reply.status(StatusCodes.OK)
                .send({ status: 'ok' });
        } catch (error: any) {
            request.log.error(error);
            return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
                .send({
                    status: 'error',
                    error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
                });
        }
    });


    done();
}